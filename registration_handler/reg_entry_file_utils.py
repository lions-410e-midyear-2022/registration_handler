""" Helper functions to work with local reg_entry files, downloaded from mongo
"""

import json

from lions_410e_midyear_2022_common import models
from rich import print


def get_model_from_reg_entry(reg_num):
    with open(f"reg_entry_{reg_num:03}.json", "r") as fh:
        d = json.loads(fh.read())
    registration = models.Registration(**d)
    return registration
