""" Build 410E 2022 Midyear Conference registration record markdown and PDF files from a supplied Registration model
"""


import attr
import json
import os, os.path
from pathlib import Path

import reg_entry_file_utils

import click
from lions_410e_midyear_2022_common import models, constants, build_pdf
from rich import print


@attr.s
class RegistrationRenderer(object):
    registration = attr.ib(models.Registration)
    out_dir = attr.ib(default=None)

    def __payment_details(self):
        self.out.append(f"# Payment Details {{-}}")
        self.out.append(
            f"""\

Please make all payments to this account:

* **Bank**: Nedbank
* **Branch Code**: 198765
* **Account Number**: 1232606111
* **Account Name**: LIONS CLUB OF WILROPARK -ACC LIONS CLARENS CONFERENCE
* **Account Type**: Current Account


Please make EFT payments rather than cash deposits wherever possible.

Use the reference "*{self.registration.reg_num_string}*" when making payment. 

Please send proof of payment to [midyear@lions410e.org.za](mailto:midyear@lions410e.org.za).

## Accommodation {{-}}

Accommodation bookings must be made directly with Golden Gate National Park Hotel & Chalets.  Bookings open 26 July 2022 on a first come first served basis.  Call 058 255 1000 and quote reference no. 45793.

## Cancellations {{-}}

* If your registration is cancelled earlier than 21 days before your date of arrival, your payment will be refunded in full except for a R50 admin fee.
* Cancellations later than 21 days before your date of arrival will not be refunded as the full expenses will already have been incurred for the registration.

Thank you again for registering for the 2022 District 410E Midyear Conference.
"""
        )

    def __attrs_post_init__(self):
        r = self.registration
        self.out = [
            f"# Registration Number: {r.reg_num_string} {{-}}",
        ]

        self.out.append(
            f"## Attendee Details - Registered on {r.timestamp:%d/%m/%y at %H:%M} {{-}} "
        )
        self.append(f"* **First Name(s):** {r.first_names}")
        self.append(f"* **Last Name:** {r.last_name}")
        if r.cell:
            self.append(f"* **Cell Phone:** {r.cell}")
        if r.email:
            self.append(f"* **Email Address:** {r.email}")
        self.append(f"* **Club:** {r.club}")
        self.append(f"* **Dietary Requirements:** {r.dietary if r.dietary else 'None'}")
        self.append(f"* **Disabilities:** {r.disability if r.disability else 'None'}")
        self.append(f"* **Details On Name Badge:** {r.name_badge}")
        if r.auto_name_badge:
            self.append("")
            self.append(
                f"**The name badge details were generated from the first and last names because no name badge details were supplied on the registration form. Please contact the registration team if you would like to update these details.**"
            )
        self.append(
            f"* **Attendee is a {'Lions member' if r.lion else 'Partner in Service or child'}**"
        )

        self.out.append("")
        self.out.append("")
        self.out.append(f"# Total Cost: R{r.cost} {{-}}")
        self.out.append("")
        self.out.append("")
        self.__payment_details()
        self.name = r.full_name.lower().replace(" ", "_").lower()
        fn_name = self.name.replace('"', "").replace("'", "")
        self.fn = f"midyear_2022_410e_registration_{self.registration.reg_num:003}_{fn_name}.txt"
        if self.out_dir:
            self.fn = os.path.join(self.out_dir, self.fn)
        self.save()

    def append(self, msg):
        self.out.append(msg.encode("ascii", "ignore").decode("ascii"))

    def save(self):
        with open(self.fn, "w") as fh:
            fh.write("\n".join(self.out))


def build_pdf_from_model(registration_model, debug=False, out_dir="."):
    renderer = RegistrationRenderer(registration_model, out_dir)
    if debug:
        print(registration_model)
        print(renderer.fn)
    if out_dir == ".":
        out_dir = os.getcwd()
    pdf_fn = build_pdf.build_pdf(out_dir, renderer.fn, pull=False, debug=False)
    if debug:
        print(pdf_fn)
    return Path(pdf_fn)


@click.command()
@click.argument("reg_num", type=int)
def build_pdf_from_reg_entry(reg_num):
    registration = reg_entry_file_utils.get_model_from_reg_entry(reg_num)
    pth = build_pdf_from_model(registration, out_dir=".")
    print(f"Registration record for reg num {reg_num:03} written to {pth}")


if __name__ == "__main__":
    build_pdf_from_reg_entry()
