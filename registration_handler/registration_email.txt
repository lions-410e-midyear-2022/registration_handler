Good day,
<p>
Thank you for registering for the District 410E 2022 Midyear Conference at the Golden Gate Hotel near Clarens from 28 to 30 October 2022.
<p>
Please use this registration number when making payments or queries: <strong>{registration.reg_num_string}</strong>.
<p>
Please see attached for a record of your registration. If any of these details are incorrect, please contact Kim van Wyk on vanwykk@gmail.com or 083 384 4260 (preferably by WhatsApp or SMS rather than voice call if possible).
<p>
For general queries on the Midyear conference, please contact the organising committee on midyear@lions410e.org.za. <b>Please do not reply directly to this email</b> - your reply is unlikely to be received.
<p>
News and details for the conference will be published on the conference website: <a href="https://midyear.lions410e.org.za/">midyear.lions410e.org.za</a>.
<p>
Thank you<br>
The District 410E 2022 Midyear Conference Organising Committee
