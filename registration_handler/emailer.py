from collections import namedtuple
from pathlib import Path

import click
from lions_410e_midyear_2022_common import emailer
from rich import print

import reg_entry_file_utils

REGISTRATION_RECORD = namedtuple(
    "RegistrationRecord", ("registration_model", "pdf_path")
)


def send_reg_email(registration_record):
    with open(
        f"registration_email.txt",
        "r",
    ) as fh:
        template = fh.read()

    reg = registration_record.registration_model
    if reg.email:
        emailer.send_mail(
            reg.email,
            f"Confirmation of registration {reg.reg_num_string} for the Lions District 410E 2022 Midyear Conference for {reg.full_name}",
            template.format(registration=reg),
            [registration_record.pdf_path],
        )
    else:
        # no email addresses available
        emailer.send_mail(
            emailer.BCCS,
            f"2022 Midyear - no email address for {reg.reg_num_string}",
            "Submitted details attached.",
            [registration_record.pdf_path],
        )


@click.command()
@click.argument("reg_num", type=int)
@click.argument("pdf_path")
def email_reg_record(reg_num, pdf_path):
    registration = reg_entry_file_utils.get_model_from_reg_entry(reg_num)
    send_reg_email(REGISTRATION_RECORD(registration, Path(pdf_path)))


if __name__ == "__main__":
    email_reg_record()
