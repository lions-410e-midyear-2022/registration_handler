from collections import namedtuple
import copy
import json
import os
import sys
from time import sleep

from lions_410e_midyear_2022_common import models, sqs
from rich import print

import create_reg_record
import emailer
import mongo


REGISTRATION_RECORD = namedtuple(
    "RegistrationRecord", ("registration_model", "pdf_path")
)


def process_reg_form_dict(reg_form_dict, debug=False):
    try:
        registration = models.Registration(**reg_form_dict)
        pdf_path = create_reg_record.build_pdf_from_model(registration, debug=debug)
        reg_record = REGISTRATION_RECORD(registration, pdf_path)
        if debug:
            print(f"Reg record: {reg_record}")
        emailer.send_reg_email(reg_record)
    except Exception as e:
        print(e)
        pass


def process_reg_form_submissions(reg_form_data_list, test=False, debug=False):
    reg_num = mongo.get_next_reg_num()
    for reg_form_data in reg_form_data_list:
        try:
            reg_form_dict = json.loads(reg_form_data)
            if debug:
                print(f"Reg form dict: {reg_form_dict}")
            reg_form_dict["reg_num"] = reg_num
            if not test:
                mongo.insert_reg_form(reg_form_dict)
                if debug:
                    print("Inserted into mongo db")
            process_reg_form_dict(reg_form_dict, debug=debug)
            reg_num += 1
            sleep(0.3)
        except Exception as e:
            print(e)
            pass


def main(test_file=None, debug=False):
    if test_file:
        debug = True
        print(test_file)
        with open(test_file, "r") as fh:
            reg_form_data_list = [fh.read()]
    else:
        reg_form_data_list = sqs.read_reg_form_data(max_number_of_messages=10)
    process_reg_form_submissions(
        reg_form_data_list,
        test=test_file is not None,
        debug=debug,
    )


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        "District 410E Midyear 2022 Conference Registration Handler, processing entries from an SQS queue"
    )
    parser.add_argument("-v", action="store_true", help="Emit debug")
    parser.add_argument(
        "--test-file",
        default=None,
        help="Use provided test data rather than the SQS queue",
    )
    parser.add_argument(
        "--schedule-period",
        type=int,
        default=None,
        help="A period of time in seconds to wait before processing the queue again. If not given, the tool runs once and exits",
    )

    args = parser.parse_args()
    main(args.test_file, args.v)
    if args.schedule_period is not None:
        import schedule
        import time

        schedule.every(args.schedule_period).seconds.do(
            main, test_file=args.test_file, debug=args.v
        )
        while True:
            schedule.run_pending()
            time.sleep(1)
