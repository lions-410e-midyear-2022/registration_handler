import mongo
import registration_handler

import click

from time import sleep


@click.command()
@click.argument("reg_nums", type=int, nargs=-1)
def process_reg_nums(reg_nums: [int]):
    for reg_num in reg_nums:
        reg_form_dict = mongo.get_reg_entry_dict(reg_num)
        registration_handler.process_reg_form_dict(reg_form_dict, debug=True)
        sleep(0.5)


if __name__ == "__main__":
    process_reg_nums()
