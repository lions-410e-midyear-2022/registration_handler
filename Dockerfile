FROM registry.gitlab.com/kimvanwyk/python3-poetry

# /app is the workdir. Copy files to execute to /app/
COPY ./registration_handler /app/

ENTRYPOINT ["python", "-u", "registration_handler.py"]
